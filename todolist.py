from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date
from datetime import datetime, timedelta
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///todo.db?check_same_thread=False')
Base = declarative_base()


class Table(Base):
    __tablename__ = 'task'
    id = Column(Integer, primary_key=True)
    task = Column(String)
    deadline = Column(Date, default=datetime.today())

    def __repr__(self):
        return self.task


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


def add_task_to_db(user_task, user_deadline):
    new_row = Table(task=user_task, deadline=user_deadline)
    session.add(new_row)
    session.commit()


def print_tasks(rows):
    x = 1
    for i in rows:
        print(f"{x}. {i.id} {i.task}. {i.deadline.strftime('%#d %#b')}")
        x += 1


def get_today_task():
    today = datetime.today()
    rows = session.query(Table).filter(Table.deadline == today.date()).order_by(Table.deadline).all()
    if not rows:
        print("Nothing to do!")
    else:
        print_tasks(rows)


def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days + 1)):
        yield start_date + timedelta(n)


def get_weeks_tasks():
    weekdays = [
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday']
    today = datetime.now()
    end_of_week = today + timedelta(days=6)
    for single_date in date_range(today, end_of_week):
        # x = 1
        print()
        print(weekdays[single_date.weekday()].capitalize(), single_date.strftime("%#d %#b"))
        rows = session.query(Table).filter(Table.deadline == single_date.date()).order_by(Table.deadline).all()
        if not rows:
            print("Nothing to do!")
        else:
            print_tasks(rows)


def get_all_tasks():
    rows = session.query(Table).order_by(Table.deadline).all()
    if not rows:
        print("Nothing to do!")
    else:
        print_tasks(rows)


def get_missed_tasks():
    rows = session.query(Table).filter(Table.deadline < datetime.today().date()).order_by(Table.deadline).all()
    if not rows:
        print("Nothing is missed!")
    else:
        print_tasks(rows)


def delete_task():
    # rows = session.query(Table).filter(Table.deadline < datetime.today().date()).order_by(Table.deadline).all()
    rows = session.query(Table).order_by(Table.deadline).all()
    if not rows:
        print("Nothing to delete")
    else:
        print("Choose the number of the task you want to delete:")
        print_tasks(rows)
        del_task = int(input("> "))
        specific_row = rows[del_task - 1]
        session.delete(specific_row)
        session.commit()
        print("The task has been deleted!")


menu = """
1) Today's tasks
2) Week's tasks
3) All tasks
4) Missed tasks
5) Add task
6) Delete task
0) Exit"""

while True:
    print(menu)
    user_input = input('> ')
    if user_input == "1":
        print(f"Today {datetime.today().strftime('%d %b')}")
        get_today_task()
    elif user_input == "2":
        get_weeks_tasks()
    elif user_input == "3":
        print()
        print("All tasks:")
        get_all_tasks()
    elif user_input == "4":
        print()
        print("Missed tasks:")
        get_missed_tasks()
    elif user_input == "5":
        print()
        print("Enter task")
        user_input = input("> ")
        print("Enter deadline")
        user_input_deadline = datetime.strptime(input("> "), '%Y-%m-%d')
        add_task_to_db(user_input, user_input_deadline)
        print('The task has been added!')
    elif user_input == "6":
        print()
        delete_task()
    elif user_input == "0":
        exit("Bye!")
    else:
        print("Wrong input")
